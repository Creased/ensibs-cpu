#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Basic instruction parser."""

# Generate documentation: epydoc -v --html parser.py -o ./docs
# Make tests: python3 -m doctest -v parser.py

import argparse
import re

def to_binary(instruction):
    """Convert basic instruction to binary.

    >>> instruction = 'LDR R1 0'
    >>> to_binary(instruction)
    '0100'

    >>> instruction = 'XOR R3 R3 R2'
    >>> to_binary(instruction)
    '00a9'

    >>> instruction = 'XOR R5 R4 R3'
    >>> to_binary(instruction)
    Traceback (most recent call last):
    ...
    ValueError: Bad store register value specified!

    @param instruction: basic instruction.
    """
    opcode_set = {
        'AND': '000',
        'OR':  '001',
        'XOR': '010',
        'ADD': '011',
        'LDR': '100'
    }

    register_set = {
        'R1': '00',
        'R2': '01',
        'R3': '10',
        'R4': '11'
    }

    max_const = 15

    instruction = re.sub('[ ][ ]*', ' ', instruction)
    instruction = re.sub('[\t ]*//.*$', '', instruction)
    instruction = re.sub('[ ]$', '', instruction)

    instruction_parts = instruction.split(' ')

    if len(instruction_parts) == 3:
        (opcode,
         store_register,
         constant) = instruction.split(' ')
        a_register = b_register = ''
    elif len(instruction_parts) == 4:
        (opcode,
         store_register,
         a_register,
         b_register) = instruction.split(' ')
        constant = ''
    else:
        raise ValueError('Bad instruction specified, please check format!')

    if opcode not in opcode_set:
        raise ValueError('Bad opcode value specified!')
    elif store_register not in register_set:
        raise ValueError('Bad store register value specified!')
    elif a_register not in register_set and a_register != '':
        raise ValueError('Bad A register value specified!')
    elif b_register not in register_set and b_register != '':
        raise ValueError('Bad B register value specified!')
    elif constant != '' and int(constant) > max_const:
        raise ValueError('Constant must be lower than 15!')
    else:
        if constant == '':
            opcode_bits = '{a}{b}'.format(a=register_set[a_register],
                                          b=register_set[b_register])
        else:
            constant = str(bin(int(constant))[2:].zfill(len(bin(max_const)[2:])))
            opcode_bits = constant

        payload = ('{opcode}'
                   '{store_register}'
                   '{opcode_bits}').format(opcode=opcode_set[opcode],
                                           store_register=register_set[store_register],
                                           opcode_bits=opcode_bits)

        # print(payload)
        payload = hex(int(payload, 2))[2:].zfill(4)

        return payload

def write_image(payload, output_file):
    """Write payload to image file.

    @param payload: binary instruction.
    """
    with open(output_file, 'wt') as image_fd:
        image_fd.write('v2.0 raw\n')
        image_fd.write(payload)

def chunks(str_, step):
    """Chunks formatter.

    >>> str_ = 'Hello, World!'
    >>> chunks(str_, 4)
    ['Hell', 'o, W', 'orld', '!']

    @param str_: string to process.
    @param step: chunk size.
    """
    step = max(1, step)
    chunk = list()

    for i in range(0, len(str_), step):
        chunk += [str_[i:i + step]]

    return chunk

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser(description='Basic instruction parser')

    parser.add_argument('-i', '--input',
                        type=str,
                        default='instruction.txt',
                        help='instruction')

    parser.add_argument('-o', '--output',
                        type=str,
                        default='instruction.bin',
                        help='binary')

    args = parser.parse_args()

    return args

def main():
    """Convert pseudo-ASM code to binary file."""
    try:
        # Arguments parsing
        args = parse_args()

        # Load config
        input_file = args.input
        output_file = args.output

        payload = ''

        try:
            with open(input_file, 'rt') as code_fd:
                lines = code_fd.read().splitlines()
                for line in lines:
                    payload += to_binary(line)
        except FileNotFoundError:
            raise FileNotFoundError('Specified file not found!')
        else:
            payload = ' '.join(chunks(payload, 4))
            write_image(payload, output_file)
    except (TypeError, ValueError, FileNotFoundError) as exception_:
        print('[!] {}'.format(exception_))

# Runtime processor
if __name__ == '__main__':
    main()
