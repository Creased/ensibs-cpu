# TP CPU ENSIBS

L'objectif de ce TP est de s'initier à la conception de circuits logiques par la création
d'une unité centrale de traitement (CPU) de 4 bits.

![CPU](illustrations/counter.gif)

## Exploitation

⚠ Ce projet est réalisé en utilisant le logiciel [Logisim](http://www.cburch.com/logisim/).
L'interpréteur de pseudo-code ASM (très simplifié) est quant à lui développé en utilisant Python3.

## Documentation

La documentation du projet est disponible dans le [rapport de projet](rapport_projet.pdf).

La documentation de l'interpréteur est générée au moyen de Epydoc et accessible depuis
le répertoire [docs/](docs) (voir le fichier [docs/index.html](docs/index.html) pour accéder
à la table des matières).
